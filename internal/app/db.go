package app

import (
	"database/sql"
	"log"
)

func touchDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (app *Application) ConnectDB() (*sql.DB, error) {
	conn, err := touchDB(app.DSN)
	if err != nil {
		return nil, err
	}

	log.Println("Connected to DB.")
	return conn, nil
}
