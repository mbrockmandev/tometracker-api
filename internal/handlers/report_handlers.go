package handlers

import (
	"fmt"
	"net/http"

	"github.com/mbrockmandev/tometracker/internal/jsonHelper"
)

type BookReport struct {
	ID          int `json:"id"`
	Title       int `json:"title"`
	Author      int `json:"author"`
	BorrowCount int `json:"borrow_count"`
}

func (h *Handler) ReportPopularBooks(w http.ResponseWriter,
	r *http.Request) {
	books, err := h.App.DB.ReportPopularBooks()
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("failed to retrieve popular books report: %s", err), http.StatusBadRequest)
		return
	}

	if books == nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("no books found"), http.StatusNotFound)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, books)
}

func (h *Handler) ReportPopularGenres(w http.ResponseWriter,
	r *http.Request) {
	genres, err := h.App.DB.ReportPopularGenres()
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("failed to retrieve popular genres report: %s", err), http.StatusBadRequest)
		return
	}

	if genres == nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("no genres found"), http.StatusNotFound)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, genres)
}

func (h *Handler) ReportBusyTimes(w http.ResponseWriter,
	r *http.Request) {
	busyTimes, err := h.App.DB.ReportBusyTimes()
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("failed to retrieve busy times report: %s", err), http.StatusBadRequest)
		return
	}

	if busyTimes == nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("no times found"), http.StatusNotFound)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, busyTimes)
}
