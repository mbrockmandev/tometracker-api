package handlers

import (
	"net/http"

	"github.com/mbrockmandev/tometracker/internal/app"
)

type Handler struct {
	App *app.Application
}

type AppHandlers interface {
	// Test Handlers
	Home(http.ResponseWriter, *http.Request)

	// Auth Handlers
	RegisterNewUser(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
	GetRefreshToken(http.ResponseWriter, *http.Request)
	Logout(http.ResponseWriter, *http.Request)
	TestRefresh(http.ResponseWriter, *http.Request)

	// Book Handlers
	GetBookById(http.ResponseWriter, *http.Request)
	GetBookByIsbn(http.ResponseWriter, *http.Request)
	GetBooksByQuery(http.ResponseWriter, *http.Request)
	GetBooksByAuthor(http.ResponseWriter, *http.Request)
	BorrowBook(http.ResponseWriter, *http.Request)
	ReturnBook(http.ResponseWriter, *http.Request)
	CreateBook(http.ResponseWriter, *http.Request)
	UpdateBook(http.ResponseWriter, *http.Request)
	DeleteBook(http.ResponseWriter, *http.Request)

	// User Handlers
	GetUserById(http.ResponseWriter, *http.Request)
	GetUserByEmail(http.ResponseWriter, *http.Request)
	CreateUser(http.ResponseWriter, *http.Request)
	UpdateUser(http.ResponseWriter, *http.Request)
	DeleteUser(http.ResponseWriter, *http.Request)

	// Report Handlers
	ReportPopularBooks(http.ResponseWriter, *http.Request)
	ReportPopularGenres(http.ResponseWriter, *http.Request)
	ReportBusyTimes(http.ResponseWriter, *http.Request)

	// Middleware
	EnableCors(h http.Handler) http.Handler
	RequireAuth(h http.Handler) http.Handler
	RequireRole(reqRole string) func(next http.Handler) http.Handler

	// Recommender
	RecommendedBooks(http.ResponseWriter, *http.Request)
}
