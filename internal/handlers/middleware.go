package handlers

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/mbrockmandev/tometracker/internal/jsonHelper"
)

func (h *Handler) EnableCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
		w.Header().Set("Access-Control-Allow-Credentials", "true")

		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE,OPTIONS")
			w.Header().
				Set("Access-Control-Allow-Headers", "Accept, Content-Type, X-CSRF-Token, Authorization, Credentials")
			w.WriteHeader(http.StatusNoContent)
			return
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func (h *Handler) RequireAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _, err := h.App.Auth.GetTokenAndVerify(w, r)
		if err != nil {
			jsonHelper.ErrorJson(w, fmt.Errorf("unauthorized: %s", err), http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (h *Handler) RequireRole(reqRole string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			authHeader := r.Header.Get("Authorization")
			if authHeader == "" {
				jsonHelper.ErrorJson(w, errors.New("no auth header provided"), http.StatusUnauthorized)
				return
			}

			tokenStr := strings.Split(authHeader, " ")[1]
			role, err := h.App.Auth.GetUserRoleFromJWT(tokenStr)
			if err != nil || role != reqRole {
				jsonHelper.ErrorJson(w, fmt.Errorf("unauthorized: %s", err), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}
