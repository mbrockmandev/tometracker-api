package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/mbrockmandev/tometracker/internal/jsonHelper"
	"github.com/mbrockmandev/tometracker/internal/models"
)

// debug only
func (h *Handler) Home(w http.ResponseWriter, r *http.Request) {
	payload := struct {
		Status  string `json:"status"`
		Message string `json:"message"`
		Version string `json:"version"`
	}{
		Status:  "active",
		Message: "TomeTracker is running!",
		Version: "0.0.1",
	}

	jsonHelper.WriteJson(w, http.StatusOK, payload)
}

func (h *Handler) CreateBook(w http.ResponseWriter,
	r *http.Request) {
	var req struct {
		Book      models.Book `json:"book"`
		LibraryId int         `json:"library_id"`
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusBadRequest)
		return
	}

	_, err = h.App.DB.CreateBook(&req.Book, req.LibraryId)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
		return
	}

	jsonHelper.WriteJson(w, http.StatusCreated, map[string]string{"message": "Book created successfully."})
}

func (h *Handler) GetBookById(w http.ResponseWriter,
	r *http.Request) {
	bookId := chi.URLParam(r, "id")
	if bookId == "" {
		jsonHelper.ErrorJson(w, fmt.Errorf("book id is required"), http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(bookId)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid book id format"), http.StatusBadRequest)
		return
	}

	book, err := h.App.DB.GetBookById(id)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("failed to retrieve book with id %v", id), http.StatusBadRequest)
		return
	}

	if book == nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("book not found"), http.StatusNotFound)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, book)
}

func (h *Handler) GetBookByIsbn(w http.ResponseWriter,
	r *http.Request) {
	isbn := chi.URLParam(r, "isbn")
	if isbn == "" {
		jsonHelper.ErrorJson(w, fmt.Errorf("isbn is required"), http.StatusBadRequest)
		return
	}

	book, err := h.App.DB.GetBookByIsbn(isbn)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("failed to retrieve book with isbn %s", isbn), http.StatusBadRequest)
		return
	}

	if book == nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("book not found"), http.StatusNotFound)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, book)
}

func (h *Handler) GetBooksByQuery(w http.ResponseWriter,
	r *http.Request) {
	query := chi.URLParam(r, "query")
	if query == "" {
		jsonHelper.ErrorJson(w, fmt.Errorf("book id is required"), http.StatusBadRequest)
		return
	}

	books, err := h.App.DB.GetBooksByQuery(query)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("unable to complete the request: %s", err), http.StatusBadRequest)
	}
	// change this logic?
	if books == nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("no results found"), http.StatusNotFound)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, books)
}

func (h *Handler) GetBooksByAuthor(w http.ResponseWriter,
	r *http.Request) {
	author := chi.URLParam(r, "author")
	if author == "" {
		jsonHelper.ErrorJson(w, fmt.Errorf("author is required"), http.StatusBadRequest)
		return
	}

	books, err := h.App.DB.GetBooksByAuthor(author)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, books)
}

func (h *Handler) BorrowBook(w http.ResponseWriter,
	r *http.Request) {
	var req struct {
		LibraryId int `json:"library_id"`
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid request body"), http.StatusBadRequest)
		return
	}

	bookIdStr := chi.URLParam(r, "id")

	bookId, err := strconv.Atoi(bookIdStr)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid id"), http.StatusBadRequest)
	}

	userId, err := h.getUserIdFromJWT(r)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("unauthorized"), http.StatusUnauthorized)
		return
	}

	err = h.App.DB.BorrowBook(userId, bookId, req.LibraryId)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, map[string]string{"message": "Book borrowed successfully."})

}

func (h *Handler) ReturnBook(w http.ResponseWriter,
	r *http.Request) {
	var req struct {
		LibraryId int `json:"library_id"`
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid request body"), http.StatusBadRequest)
		return
	}

	bookIdStr := chi.URLParam(r, "id")
	bookId, err := strconv.Atoi(bookIdStr)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid book id"), http.StatusBadRequest)
		return
	}

	userId, err := h.getUserIdFromJWT(r)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid user id"), http.StatusBadRequest)
		return
	}

	err = h.App.DB.ReturnBook(userId, bookId, req.LibraryId)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, map[string]string{"message": "Book returned successfully."})
}

func (h *Handler) UpdateBook(w http.ResponseWriter,
	r *http.Request) {
	bookIdStr := chi.URLParam(r, "id")
	bookId, err := strconv.Atoi(bookIdStr)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid id"), http.StatusBadRequest)
		return
	}
	var book models.Book

	err = json.NewDecoder(r.Body).Decode(&book)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusBadRequest)
		return
	}

	book.ID = bookId
	err = h.App.DB.UpdateBook(bookId, &book)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, map[string]string{"message": "Book updated successfully."})

}

func (h *Handler) DeleteBook(w http.ResponseWriter,
	r *http.Request) {
	bookIdStr := chi.URLParam(r, "id")
	bookId, err := strconv.Atoi(bookIdStr)
	if err != nil {
		jsonHelper.ErrorJson(w, fmt.Errorf("invalid id"), http.StatusBadRequest)
		return
	}

	err = h.App.DB.DeleteBook(bookId)
	if err != nil {
		jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
		return
	}

	jsonHelper.WriteJson(w, http.StatusOK, map[string]string{"message": "Book updated successfully."})
}

func (h *Handler) RecommendedBooks(w http.ResponseWriter, r *http.Request) {
	// books, err := python.Run("recommended_books.py")
	// if err != nil {
	// 	jsonHelper.ErrorJson(w, err, http.StatusInternalServerError)
	// 	return
	// }

	jsonHelper.WriteJson(w, http.StatusOK, nil)
}
