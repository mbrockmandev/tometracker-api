package database

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/mbrockmandev/tometracker/internal/models"
)

func (p *PostgresDBRepo) CreateBook(book *models.Book, libraryId int) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		insert into
			books
				(title, author, isbn, published_at, summary)
			values
				($1, $2, $3, $4, $5)
		returning id
	`

	var newId int
	err := p.DB.QueryRowContext(ctx, stmt,
		book.Title,
		book.Author,
		book.ISBN,
		book.PublishedAt,
		book.Summary,
	).Scan(&newId)
	if err != nil {
		return 0, err
	}

	checkStmt := `
		select
			count(*)
		from
			books_libraries
		where
			book_id = $1 and library_id = $2
	`

	var count int
	err = p.DB.QueryRowContext(ctx, checkStmt, newId, libraryId).Scan(&count)
	if err != nil {
		return 0, err
	}

	if count == 0 {
		insertStmt := `
			insert into
				books_libraries
					(book_id, library_id, total_copies, available_copies)
				values
					($1, $2, 1, 1)
		`

		_, err = p.DB.ExecContext(ctx, insertStmt, newId, libraryId)
		if err != nil {
			return 0, err
		}
	} else {
		updateStmt := `
			update
				books_libraries
			set
				total_copies = total_copies + 1,
				available_copies = available_copies + 1
			where
				book_id = $1 and library_id = $2
		`

		_, err = p.DB.ExecContext(ctx, updateStmt, newId, libraryId)
		if err != nil {
			return 0, err
		}
	}

	return newId, nil
}

func (p *PostgresDBRepo) GetAllBooks(genre ...int) ([]*models.Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	where := ""
	if len(genre) > 0 {
		where = fmt.Sprintf(
			"where id in (select book_id from books_genres where genre_id = %d)",
			genre[0],
		)
	}

	query := fmt.Sprintf(`
		select
		  id, title, author, isbn, published_at, summary
		from
		  books %s
		order by
			title
	`, where)

	rows, err := p.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []*models.Book

	for rows.Next() {
		book := &models.Book{}
		err := rows.Scan(
			&book.ID,
			&book.Title,
			&book.Author,
			&book.ISBN,
			&book.PublishedAt,
			&book.Summary,
		)
		if err != nil {
			return nil, err
		}

		books = append(books, book)
	}
	return books, nil
}

func (p *PostgresDBRepo) GetBookByIsbn(isbn string) (*models.Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		select
			id, title, author, isbn, published_at, summary
		from
			books
		where
			isbn = $1
	`

	row := p.DB.QueryRowContext(ctx, query, isbn)
	book := &models.Book{}
	err := row.Scan(
		&book.ID,
		&book.Title,
		&book.Author,
		&book.ISBN,
		&book.PublishedAt,
		&book.Summary,
	)
	if err != nil {
		return nil, err
	}
	return book, nil
}

func (p *PostgresDBRepo) GetBookById(id int) (*models.Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `select
	id, title, author, isbn, published_at, summary
	from books
	where id = $1`

	row := p.DB.QueryRowContext(ctx, query, id)

	book := &models.Book{}
	err := row.Scan(
		&book.ID,
		&book.Title,
		&book.Author,
		&book.ISBN,
		&book.PublishedAt,
		&book.Summary,
	)
	if err != nil {
		return nil, err
	}
	return book, nil
}

func (p *PostgresDBRepo) GetBooksByQuery(query string) ([]*models.Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		select
		  *
		from
		  books
		where
			title ilike $1
		order by
			title
	`

	searchQuery := "%" + query + "%"

	rows, err := p.DB.QueryContext(ctx, stmt, searchQuery)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []*models.Book

	for rows.Next() {
		book := &models.Book{}
		err := rows.Scan(
			&book.ID,
			&book.Title,
			&book.Author,
			&book.ISBN,
			&book.PublishedAt,
			&book.Summary,
		)
		if err != nil {
			return nil, err
		}

		books = append(books, book)
	}
	return books, nil
}

func (p *PostgresDBRepo) GetBooksByAuthor(author string) ([]*models.Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		select
			*
		from
			books
		where
			author ilike $1
		order by
			title
	`

	searchQuery := "%" + author + "%"

	rows, err := p.DB.QueryContext(ctx, stmt, searchQuery)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []*models.Book

	for rows.Next() {
		book := &models.Book{}
		err := rows.Scan(
			&book.ID,
			&book.Title,
			&book.Author,
			&book.ISBN,
			&book.PublishedAt,
			&book.Summary,
		)
		if err != nil {
			return nil, err
		}

		books = append(books, book)
	}
	return books, nil
}

func (p *PostgresDBRepo) BorrowBook(userId, bookId, libraryId int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		select
			count(*)
		from
			books_libraries
		where
			book_id = $1 and library_id = $2
	`

	var count int
	err := p.DB.QueryRowContext(ctx, stmt, bookId, libraryId).Scan(&count)
	if err != nil {
		return err
	}
	if count < 1 {
		return fmt.Errorf("book is not available at this library")
	}

	stmt = `
	select
		count(*)
	from
		users_books
	where
		user_id = $1 and book_id = $2 and returned_at is null
	`

	err = p.DB.QueryRowContext(ctx, stmt, userId, bookId).Scan(&count)
	if err != nil {
		return err
	}

	if count > 0 {
		return fmt.Errorf("user has already borrowed book with id %v and this has not been returned yet", bookId)
	}

	stmt = `
		select
			available_copies
		from
			books_libraries
		where
			id = $1
	`

	var availableCopies int
	err = p.DB.QueryRowContext(ctx, stmt, bookId).Scan(&availableCopies)
	if err != nil {
		return err
	}

	if availableCopies < 1 {
		return fmt.Errorf("no available copies of this book at this library for borrowing")
	}

	stmt = `
		update
			books_libraries
		set
			available_copies = available_copies - 1
		where
			book_id = $1 and library_id = $2
	`

	_, err = p.DB.ExecContext(ctx, stmt, bookId, libraryId)
	if err != nil {
		return err
	}

	stmt = `
		insert into
			users_books
				(user_id, book_id, due_date, borrowed_at)
			values
				($1, $2, $3, $4)
			returning id
	`

	var borrowId int
	dueDate := time.Now().AddDate(0, 0, 14)

	err = p.DB.QueryRowContext(ctx, stmt, userId, bookId, dueDate, time.Now()).Scan(&borrowId)
	if err != nil {
		return err
	}

	return nil
}

func (p *PostgresDBRepo) ReturnBook(userId, bookId, libraryId int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	checkStmt := `
	select
		id
	from
		users_books
	where
		book_id = $1 and user_id = $2 and returned_at is null
	`

	var borrowId int
	err := p.DB.QueryRowContext(ctx, checkStmt, bookId, userId).Scan(&borrowId)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Errorf("the user hasn't borrowed this book or has already returned it")
		}
		return err
	}

	checkStmt = `
		select
			borrowed_copies
		from
			books_libraries
		where
			library_id = $1 and book_id = $2 and user_id = $3
	`

	var borrowedCopies int
	err = p.DB.QueryRowContext(ctx, checkStmt, libraryId, bookId, userId).Scan(&borrowedCopies)
	if err != nil {
		return err
	}
	if borrowedCopies < 1 {
		return fmt.Errorf("no copies of this book at this library for returning")
	}

	returnStmt := `
		update
			users_books
		set
			returned_at = $1
		where
			id = $2
	`

	_, err = p.DB.ExecContext(ctx, returnStmt, time.Now(), borrowId)
	if err != nil {
		return err
	}

	updateCountStmt := `
		update
			books_libraries
		set
			available_copies = available_copies + 1
		where
			id = $1
	`

	_, err = p.DB.ExecContext(ctx, updateCountStmt, bookId)
	if err != nil {
		return err
	}

	return nil
}

func (p *PostgresDBRepo) UpdateBook(id int, book *models.Book) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	_, err := p.GetBookById(id)
	if err != nil {
		return err
	}

	stmt := `
		update
			books
		set
			title = $1, author = $2, isbn = $3,
			published_at = $4, summary = $5 where id = $6
	`

	_, err = p.DB.ExecContext(ctx, stmt,
		book.Title,
		book.Author,
		book.ISBN,
		book.PublishedAt,
		book.Summary,
		id,
	)
	if err != nil {
		return err
	}
	return nil
}

func (p *PostgresDBRepo) DeleteBook(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		delete from
			books
		where
		id = $1
	`

	_, err := p.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	stmt = `
		delete from
			books_libraries
		where
			book_id = $1
	`

	_, err = p.DB.ExecContext(ctx, stmt, id)
	return err
}

func (p *PostgresDBRepo) ReportPopularBooks() ([]*models.Book, error) {
	query := `
		select
			b.id, b.title, b.author, b.isbn, b.published_at, b.summary, count(ub.book_id) as borrow_count
		from
			users_books ub
		join
			books b on ub.book_id = b.id
		where
			ub.borrowed_at >= $1
		group by
			b.id
		order by
			borrow_count desc
		limit
			10;
	`

	oneMonthAgo := time.Now().AddDate(0, -1, 0)

	rows, err := p.DB.Query(query, oneMonthAgo)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []*models.Book
	var borrowCount int
	for rows.Next() {
		var book models.Book
		if err := rows.Scan(
			&book.ID,
			&book.Title,
			&book.Author,
			&book.ISBN,
			&book.PublishedAt,
			&book.Summary,
			&borrowCount,
		); err != nil {
			return nil, err
		}
		books = append(books, &book)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return books, nil
}
