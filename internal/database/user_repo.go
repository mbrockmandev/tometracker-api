package database

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/mbrockmandev/tometracker/internal/models"
)

func (p *PostgresDBRepo) CreateUser(user *models.User) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	currentTime := time.Now()

	query := `
		insert into
			users
				(email, first_name, last_name, password, role, created_at, updated_at)
			values
				($1, $2, $3, $4, $5, $6, $7)
		returning id
	`

	var userId int
	err := p.DB.QueryRowContext(ctx, query,
		user.Email,
		user.FirstName,
		user.LastName,
		user.Password,
		user.Role,
		currentTime,
		currentTime,
	).Scan(&userId)
	if err != nil {
		return 0, err
	}

	return userId, nil
}

func (p *PostgresDBRepo) GetAllUsers() ([]*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		select
			*
		from
			users
		order by
			last_name
	`

	rows, err := p.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []*models.User{}
	for rows.Next() {
		user := &models.User{}
		err = rows.Scan(
			&user.ID,
			&user.Email,
			&user.FirstName,
			&user.LastName,
			&user.Password,
			&user.Role,
			&user.CreatedAt,
			&user.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return users, nil
}

func (p *PostgresDBRepo) GetUserByEmail(email string) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		select
			id, email, first_name, last_name, password, role, created_at, updated_at
	 	from
	 		users
	 	where
			email = $1
	`

	user := &models.User{}
	row := p.DB.QueryRowContext(ctx, query, email)

	err := row.Scan(
		&user.ID,
		&user.Email,
		&user.FirstName,
		&user.LastName,
		&user.Password,
		&user.Role,
		&user.CreatedAt,
		&user.UpdatedAt,
	)
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("no user found with email %s", email)
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

func (p *PostgresDBRepo) GetUserRoleByEmail(email string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		select
			role
		from
			users
		where
			email = $1
	`

	row := p.DB.QueryRowContext(ctx, query, email)

	var role string
	err := row.Scan(&role)
	if err != nil {
		return "", err
	}

	return role, nil
}

func (p *PostgresDBRepo) GetUserById(id int) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
	select
		id, email, first_name, last_name, password, role, created_at, updated_at
	from
		users
	where
		id = $1`

	user := &models.User{}
	row := p.DB.QueryRowContext(ctx, query, id)

	err := row.Scan(
		&user.ID,
		&user.Email,
		&user.FirstName,
		&user.LastName,
		&user.Password,
		&user.Role,
		&user.CreatedAt,
		&user.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (p *PostgresDBRepo) UpdateUser(id int, user *models.User) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	_, err := p.GetUserById(id)
	if err != nil {
		return err
	}

	query := `
		update
			users
		set
			email = $1, first_name = $2, last_name = $3, password = $4,
			role = $5, updated_at = $6 where id = $7
	`

	result, err := p.DB.ExecContext(ctx, query,
		user.Email,
		user.FirstName,
		user.LastName,
		user.Password,
		user.Role,
		user.UpdatedAt,
		&id,
	)
	if err != nil {
		return err
	}

	rowsAffected, _ := result.RowsAffected()
	if rowsAffected == 0 {
		return errors.New("no rows updated")
	}

	return nil
}

func (p *PostgresDBRepo) DeleteUser(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		delete from
			users
		where
			id = $1
	`

	_, err := p.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}
	return nil
}
