package database

import (
	"context"

	"github.com/mbrockmandev/tometracker/internal/models"
)

func (p *PostgresDBRepo) CreateLibrary(library *models.Library) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		insert into
			libraries
				(name, city, street_address, postal_code, country, phone)
			values
				($1, $2, $3, $4, $5, $6)
			returning id
	`

	var newId int
	err := p.DB.QueryRowContext(ctx, stmt,
		library.Name,
		library.City,
		library.StreetAddress,
		library.PostalCode,
		library.Country,
		library.Phone,
	).Scan(&newId)
	if err != nil {
		return 0, err
	}

	return newId, nil
}

func (p *PostgresDBRepo) GetLibraryByName(name string) (*models.Library, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		select
			id, name, city, street_address, postal_code, country, phone
		from
			libraries
		where
			name = $1
	`

	row := p.DB.QueryRowContext(ctx, query, name)
	var library models.Library

	err := row.Scan(
		&library.ID,
		&library.Name,
		&library.City,
		&library.StreetAddress,
		&library.PostalCode,
		&library.Country,
		&library.Phone,
	)
	if err != nil {
		return nil, err
	}

	return &library, nil
}

func (p *PostgresDBRepo) GetAllLibraries() ([]*models.Library, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	rows, err := p.DB.QueryContext(ctx, "select * from libraries")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var libraries []*models.Library
	for rows.Next() {
		var library models.Library
		err := rows.Scan(
			&library.ID,
			&library.Name,
			&library.City,
			&library.StreetAddress,
			&library.PostalCode,
			&library.Country,
			&library.Phone,
		)
		if err != nil {
			return nil, err
		}
	}

	return libraries, nil
}

func (p *PostgresDBRepo) GetLibraryById(id int) (*models.Library, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	query := `
		select
			id, name, city, street_address, postal_code, country, phone
		from
			libraries
		where
			id = $1
	`

	row := p.DB.QueryRowContext(ctx, query, id)
	var library models.Library
	err := row.Scan(
		&library.ID,
		&library.Name,
		&library.City,
		&library.StreetAddress,
		&library.PostalCode,
		&library.Country,
		&library.Phone,
	)
	if err != nil {
		return nil, err
	}

	return &library, nil
}

func (p *PostgresDBRepo) UpdateLibrary(id int, library *models.Library) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		update
			libraries
		set
			name = $1, city = $2, street_address = $3, postal_code = $4, country = $5, phone = $6
	`

	_, err := p.DB.ExecContext(ctx, stmt,
		library.Name,
		library.City,
		library.StreetAddress,
		library.PostalCode,
		library.Country,
		library.Phone,
	)

	return err
}

func (p *PostgresDBRepo) DeleteLibrary(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `
		delete from
			libraries
		where
			id = $1
	`

	_, err := p.DB.ExecContext(ctx, stmt, id)
	return err
}
