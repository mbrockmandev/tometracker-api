package routes

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/mbrockmandev/tometracker/internal/handlers"
)

func NewRouter(h handlers.AppHandlers) http.Handler {
	r := chi.NewRouter()

	// middleware
	r.Use(h.EnableCors)
	r.Use(middleware.Recoverer)
	// add csrf protection?
	// r.Use(h.PreventRepeatedAccess) // future- rate limit

	r.Post("/register", h.RegisterNewUser)
	r.Post("/login", h.Login)
	r.Get("/logout", h.Logout)
	r.Get("/refresh", h.GetRefreshToken)
	r.Get("/testrefresh", h.TestRefresh)

	// book routes
	r.Get("/books/{id}", h.GetBookById)
	r.Get("/books/isbn/{isbn}", h.GetBookByIsbn)
	r.Get("/books/author/{author}", h.GetBooksByAuthor)
	r.Get("/books/search/{query}", h.GetBooksByQuery)
	r.Post("/books/{id}/borrow", h.BorrowBook)
	// r.Get("/recommendedBooks", h.RecommendedBooks)

	r.Route("/admin", func(r chi.Router) {
		r.Use(h.RequireRole("admin"))
		r.Get("/users/{email}", h.GetUserByEmail)
		r.Put("/users/{id}", h.UpdateUser)
		r.Delete("/users/{id}", h.DeleteUser)
		r.Post("/users", h.CreateUser)
	})

	r.Route("/staff", func(r chi.Router) {
		r.Use(h.RequireRole("staff"))
		r.Post("/books", h.CreateBook)
		r.Put("/books/{id}", h.UpdateBook)
		r.Delete("/books/{id}", h.DeleteBook)
		r.Put("/books/{id}/return", h.ReturnBook)
		r.Get("/reports/popularBooks", h.ReportPopularBooks)
		r.Get("/reports/popularGenres", h.ReportPopularGenres)
		r.Get("/reports/peakTimes", h.ReportBusyTimes)
	})

	return r
}
