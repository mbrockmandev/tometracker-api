package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"
	_ "github.com/joho/godotenv/autoload"

	"github.com/mbrockmandev/tometracker/internal/app"
	"github.com/mbrockmandev/tometracker/internal/auth"
	db "github.com/mbrockmandev/tometracker/internal/database"
	"github.com/mbrockmandev/tometracker/internal/handlers"
	"github.com/mbrockmandev/tometracker/internal/routes"
)

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
	SSLMode  string
}

func main() {
	var application app.Application

	env := os.Getenv("ENV")
	var port, dbHost, dbPort, dbUsername, dbPassword, dbName, sslmode string

	application.APIKey = os.Getenv("API_KEY")
	application.JWTSecret = os.Getenv("JWT_SECRET")
	application.JWTIssuer = os.Getenv("JWT_ISSUER")
	application.JWTAudience = os.Getenv("JWT_AUDIENCE")
	application.CookieDomain = os.Getenv("COOKIE_DOMAIN")

	// Debug mode (uses Docker+PostgreSQL)
	if env == "debug" {
		port = os.Getenv("PORT")
		dbHost = os.Getenv("DB_HOST")
		dbPort = os.Getenv("DB_PORT")
		dbUsername = os.Getenv("DB_USERNAME")
		dbPassword = os.Getenv("DB_PASSWORD")
		dbName = os.Getenv("DB_DBNAME")
		sslmode = "disable"

		fmt.Println("Good luck debugging!")

		application.DSN = fmt.Sprintf(
			"host=%s port=%s user=%s password=%s database=%s sslmode=%s timezone=UTC connect_timeout=5",
			dbHost,
			dbPort,
			dbUsername,
			dbPassword,
			dbName,
			sslmode,
		)

		// Production Mode (uses RDS)
	} else if env == "prod" {
		port = os.Getenv("PORT")
		dbHost = os.Getenv("RDS_ENDPOINT")
		dbPort = os.Getenv("DB_PORT")
		dbUsername = os.Getenv("RDS_USERNAME")
		dbPassword = os.Getenv("RDS_PASSWORD")
		dbName = os.Getenv("DB_DBNAME")
		sslmode = "require"

		fmt.Println("Good luck in production!")

		application.DSN = fmt.Sprintf(
			"host=%s port=%s user=%s password=%s database=%s sslmode=%s timezone=UTC connect_timeout=5",
			dbHost,
			dbPort,
			dbUsername,
			dbPassword,
			dbName,
			sslmode,
		)
	}

	application.Auth = auth.Auth{
		Issuer:        application.JWTIssuer,
		Audience:      application.JWTAudience,
		Secret:        application.JWTSecret,
		TokenExpiry:   time.Minute * 15,
		RefreshExpiry: time.Hour * 24,
		CookiePath:    "/",
		CookieName:    "__Host-refresh_token",
		// CookieDomain:  application.CookieDomain,
	}

	h := &handlers.Handler{
		App: &application,
	}

	conn, err := application.ConnectDB()
	if err != nil {
		log.Fatalf("Failed to connect to database with DSN %s: %v", application.DSN, err)
	}

	application.DB = &db.PostgresDBRepo{DB: conn}
	defer application.DB.Connection().Close()

	log.Println("Starting server on port", port)

	router := routes.NewRouter(h)
	err = http.ListenAndServe(fmt.Sprintf(":%s", port), router)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
